// special instance that cares about the internal authorization of a BLO script against the rapidM2M API

const bapi = require('./bapi.js');

class bapi_blo extends bapi {
  constructor(opt) {
    opt = opt || {};
    if (opt.hasOwnProperty('auth')) {
      console.warn("sub module 'bapi-blo' uses internal authorization logic. The provided credentials are ignored.");
      delete opt.auth;
    }

    if (!process.env.BLO_SECRET) {
      console.error("sub module 'bapi-blo' is designed to be used in the BLO of rapidM2M STUDIO.");
      return;
    }

    opt.headers = opt.headers || {};
    opt.headers["Authorization"] = "Basic 3-"+Buffer.from(process.env.BLO_SECRET).toString('base64');

    opt.rejectUnauthorized = false;

    super(opt);
  }
}

module.exports = bapi_blo;