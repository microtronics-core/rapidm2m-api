"use strict";
const _ = require('underscore');
const axios = require('axios');
const https = require('https');

class bapi {
  /**
   * RapidM2M api instance
   * @param opt
   * @param {String} props.baseURL base url of the mydatanet server
   * @example https://cloud.microtronics.com
   * @param {string} props.username Username to access the api
   * @param {string} props.password Password for the requesting username
   */
  constructor(opt) {
    let axiosOpt = _.clone(opt || {});

    axiosOpt.baseURL = axiosOpt.baseURL || 'https://localhost/api/';
    axiosOpt.httpsAgent = new https.Agent({
      keepAlive: true,
      rejectUnauthorized: axiosOpt.hasOwnProperty('rejectUnauthorized') ? !!axiosOpt.rejectUnauthorized : true
    });

    if (axiosOpt.hasOwnProperty('username') || axiosOpt.hasOwnProperty('password')) {
      axiosOpt["auth"] = {
        username: axiosOpt.username,
        password: axiosOpt.password
      };
    }

    if (!axiosOpt.hasOwnProperty('timeout')) //check with 'hasOwnProperty', because it could be 0 (no timeout)
      axiosOpt.timeout = 15000;

    this.apiVersion = (opt.apiVersion || 1).toString();
    this.client = axios.create(axiosOpt);
    this.placeholders = []; //TBD
  }

  _normalizePath(path) {
    if (typeof path==="string") {
      if (path.indexOf('?') > -1)
        throw new Error('path must not include query parameters');

      path = path.split('/');
    }

    if (!path[0])
      path.shift();   // ignore any leading '/'

    if (_.isNumber(path[0]))
      path[0] = path[0].toString(); //transform first item to number string (API version)
    else
      path.unshift(this.apiVersion.toString());

    return path.map(p => {
      let prefx = (p[0] === ':' ? ':' : '');
      let mapped = (p[0] === ':' ? p.substr(1) : p);
      const v = this.placeholders[mapped];      // substitute any placeholders in the url
      return (v ? prefx + encodeURIComponent(v) : encodeURIComponent(p) );
    }).join('/');
  }

  get(opt) {
    if (typeof opt === 'string' || _.isArray(opt))
      opt = {path: opt};

    opt.method = 'get';
    return this.request(opt);
  }

  post(opt, data) {
    if (typeof opt === 'string' || _.isArray(opt)) {
      opt = {path: opt, data: data};
    }

    opt.method = 'post';
    return this.request(opt);
  }

  put(opt, data) {
    if (typeof opt === 'string' || _.isArray(opt)) {
      opt = {path: opt, data: data};
    }

    opt.method = 'put';
    return this.request(opt);
  }

  del(opt) {
    if (typeof opt === 'string' || _.isArray(opt)) {
      opt = {path: opt};
    }

    opt.method = 'delete';
    return this.request(opt);
  }

  request(opt) {
    let reqOpt = _.clone(opt);
    reqOpt.method = (reqOpt.method || 'get').toUpperCase();
    reqOpt.url = this._normalizePath(opt.path);
    delete reqOpt.path;

    if (reqOpt.method === 'GET' && _.isObject(reqOpt['query'])) {
      reqOpt.params = reqOpt.params || {};
      reqOpt.params.json = reqOpt['query'];
    }

    return new Promise(async (resolve, reject) => {
      try {
        let result = await this.client.request(reqOpt);
        resolve(result);
      } catch(e) {
        reject(e);
      }
    });
  }


  /* ---------------------------------------------------------------------------------------------------------------------
    Stamp functions
  ---------------------------------------------------------------------------------------------------------------------*/
  //Convert js Date object to rapidm2m Stamp
  dateToStamp(_date) {
    // break into stamp1+2 due to rounding effects otherwise!
    const stamp1 =
      _date.getUTCFullYear() * 10000
      + (_date.getUTCMonth() + 1) * 100 //month begins with 0
      + _date.getUTCDate();

    const stamp2 = 1000000000 +
      _date.getUTCHours() * 10000000
      + _date.getUTCMinutes() * 100000
      + _date.getUTCSeconds() * 1000
      + _date.getUTCMilliseconds();

    const stamp = stamp1.toString() + stamp2.toString().substr(1);
    return stamp.replace(/0*$/, ''); //Remove trailing zeros
  }

  /**
   * Convert times or date objects to rapidm2m timestamp
   * dt = 0/null/undefined, (ISO-)string, js Date object, number ms since 1970-01-01 0:00 UTC
   * @param {Date|String|Number} date
   * @return {String} rapidm2m timestamp
   */
  toStamp(date) {
    if (!date) return this.dateToStamp(new Date()); //current time
    if (_.isString(date)) {
      if (/^\d+$/.test(date)) return date;					//seems to be already a rapidm2m stamp value
      else return this.dateToStamp(new Date(date)); //treat as ISO stamp
    }
    if (_.isDate(date)) return this.dateToStamp(date);
    if (_.isNumber(date)) return this.dateToStamp(new Date(date));
  };


  /**
   * Convert rapidm2m timestamp to date object
   * @param {String} stamp rapidm2m stamp
   * @example 20190131123
   * @return {Date}
   */
  stampToDate(stamp) {
    const s = stamp.toString() + '00000000000000000';
    return new Date(Date.UTC(
      Number(s.substring(0, 4)),
      Number(s.substring(4, 6) - 1),
      Number(s.substring(6, 8)),
      Number(s.substring(8, 10)),
      Number(s.substring(10, 12)),
      Number(s.substring(12, 14)),
      Number(s.substring(14, 17))));
  };

  //Convert rapidm2m timestamp to unix timestamp
  /**
   * Convert a rapidm2m timestamp to a unix timestamp
   * @param {String} stamp rapidm2m timestamp
   * @return {number} unix timestamp
   */
  stampToMS(stamp) {
    const dt = this.stampToDate(stamp);
    return dt.getTime();
  };

  /**
   * Convert a rapidm2m timestamp to a date string
   * @param {String} stamp rapidm2m timestamp
   * @return {String} Date string
   */
  stampFormat(stamp) {
    const dt = this.stampToDate(stamp);
    const dtstr = dt.toISOString().replace("T", " ").replace("Z", "");

    return dt.getMilliseconds() ? dtstr : dtstr.slice(0, -5);   // remove optional [ms]
  };

  //Add or remove seconds to stamp
  /**
   * Add or remove milliseconds of a rapidm2m timestamp
   * @param {String} stamp rapidm2m timestamp
   * @param {Number} ms milliseconds to add or remove
   * @return {String} rapidm2m timestamp
   */
  stampAddMS(stamp, ms) {
    return this.toStamp(this.stampToMS(stamp) + ms);
  };

  //
  /**
   * Convert a rapidm2m timestamp to the equivalent database milliseconds. Range is 1/256 ms.
   * @param {String} stamp rapidm2m timestamp
   * @return {number} Milliseconds
   */
  stampToDBMS(stamp) {
    return Math.round(this.stampToMS(stamp) / (1000 / 256)) * 1000 / 256;
  }
}

module.exports = bapi;