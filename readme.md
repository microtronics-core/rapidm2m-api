# rapidM2M API  
This module is designed to use with the API of a rapidM2M server, but can also be used from every node.js script.   

## Features  
* Integrated authentication for Backend Logic scripts that are written in rapidM2M Studio  
* Authentication against the rapidM2M Backend API from outside of a Backend Logic script is also supported  
* [Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) API  
* Based on [axios](https://www.npmjs.com/package/axios) client
* Integrated URL encoding of path string

## Installing
Using npm:
`$ npm install rapidm2m-api`

## API
For convenience aliases are provided for the most common request methods.
##### get
`get(path[, query])`
##### put
`put(path, data)`
##### post
`post(path, data)`
##### del
`del(path, data)`
##### request
`request(config)`
Internally, all requests are processed in the API function `request`.  See the section [Request config](#request-config).

## Request config
When the function `request`is used, the config is forwarded to the internal axios client. The only exception is the field `path`, which will internally be URL encoded and then placed in the config as `url`.

See the [axios docs](https://www.npmjs.com/package/axios#request-config) for all other config options.

## Example with internal authentication (use in BLO scripts)
```javascript
//specific module for BLO scripts  
import BAPI from 'rapidm2m-api/bapi-blo';  
  
//create instance with default settings  
let bapi = new BAPI();  
  
(async () => {  
 try {  
  //only use 'data' from the result. The full result from the axios client is also available.  
  let {data: customers} = await bapi.get(['customers']);  
  
  for (let customer of customers) {  
   //change every customer's properties...  
  await bapi.put(['customers', customer._uid], {});  
  }  
 } catch (e) {  
  console.error(e);  
 }  
})();
```

## Example with provided authentication
```javascript
import BAPI from 'rapidm2m-api/bapi';  
  
//create instance  
let bapi = new BAPI({
  'username': '{user}',
  'password': '{pwd}'
});  
```